<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt"
    xmlns:rdXslExtension="urn:rdXslExtension"
>

<xsl:output method="html"
 encoding="utf-8"
 indent="no" />

<xsl:template match="/">

<HTML >
<HEAD>
<TITLE>FIO BI</TITLE><META http-equiv="X-UA-Compatible" content="IE=edge"/><LINK rel="stylesheet" type="text/css" href="rdTemplate/rdPopup/rdPopupPanel.css" />

<LINK rel="stylesheet" type="text/css" href="rdTemplate/rdTheme/Signal/Theme.css" /><META name="lgxver" content="12.6.131.1"/>
<SCRIPT type="text/javascript">(function(){if(window.external){ try {window.external.ClearOnLoad();} catch(err){return true;}; window.onerror = function myErrorHandler(errorMsg, url, lineNumber) {try{window.external.ShowMessage(errorMsg, url, lineNumber); return true;}catch(err){return false;}};}})();</SCRIPT><SCRIPT LANGUAGE="JAVASCRIPT" src="rdTemplate/rdYui.min.js?v=12.6.131.1"></SCRIPT>
<SCRIPT LANGUAGE="JAVASCRIPT" src="rdTemplate/rdScript.min.js?v=12.6.131.1"></SCRIPT>
<SCRIPT LANGUAGE="JAVASCRIPT" src="rdTemplate/rdChartCanvas.min.js?v=12.6.131.1"></SCRIPT>
<SCRIPT LANGUAGE="JAVASCRIPT" src="rdTemplate/../rdDownload/rdLocalizationInfo_4B98E452F4FD16563040E170D314DC2C.js"></SCRIPT>

<SCRIPT TYPE="text/javascript">function rdBodyLoad() {document.body.appendChild(YUI.Env.cssStampEl);
}</SCRIPT>
<SCRIPT TYPE="text/javascript">function rdValidateForm() { }</SCRIPT><SCRIPT TYPE="text/javascript" rdAjaxRunOnLoad="True"> Y.use('sessionTimeout', function(Y) { if(LogiXML.StartupScripts) { if(LogiXML.StartupScripts.onDomReady) { var currentSubscriberIndex = LogiXML.StartupScripts.onDomReady.indexOf(onDomReadyFunc); if(currentSubscriberIndex != -1) { Y.detach('domready', LogiXML.StartupScripts.onDomReady[currentSubscriberIndex]); LogiXML.StartupScripts.onDomReady.splice(currentSubscriberIndex,1); } } } function onDomReadyFunc() { LogiXML.sessionTimeout = new Y.LogiXML.SessionTimeoutControl({mode:'pinging', pingInterval:27}); }; Y.on('domready', onDomReadyFunc); if(LogiXML.StartupScripts){ if(!LogiXML.StartupScripts.onDomReady){ LogiXML.StartupScripts.onDomReady = []; } LogiXML.StartupScripts.onDomReady.push(onDomReadyFunc); } }); </SCRIPT></HEAD>
<xsl:for-each select="/rdData" ><BODY onload="rdBodyLoad()">
<FORM NAME="rdForm" method="POST">
<input type="hidden" id="rdCSRFKey1" name="rdCSRFKey" value="aa1f66e5-faa8-49fa-b4ef-a750a6f429a8" /><a  onClick="SubmitForm('rdPage.aspx?rdReport=@RequestJScript.isl~','_blank','false','',true,null)" id="actrep"><INPUT type="button" VALUE="OK" id="btnok" NAME="btnok"></INPUT>
</a><INPUT TYPE="HIDDEN" ID="rdShowElementHistory" NAME="rdShowElementHistory"></INPUT><rdHidden></rdHidden></FORM>
</BODY>
</xsl:for-each></HTML>


</xsl:template>
</xsl:stylesheet>
