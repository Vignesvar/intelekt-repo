function secure(menu,roles){
	var output = '';
	var menuLength = (menu.content)? menu.content.length: 0;
	for(var i=0; i<menuLength; i++){
		var itm = jsonToHTML(menu.content[i],roles);
		output += (itm)? itm: '';
	}
	
	return '<ul id="ActiveMenuItems" class="MenuBuilder">'+output+'</ul>';
}

function secureInactive(menu,roles){
	var output = '';
	var menuLength = (menu.content)? menu.content.length: 0;
	for(var i=0; i<menuLength; i++){
		var itm = jsonToHTML(menu.content[i],roles);
		output += (itm)? itm: '';
	}
	
	return '<ul id="InactiveMenuItems" class="MenuBuilder">'+output+'</ul>';
}

function jsonToHTML(obj,roles){
	var itm = '';
	var attr = '';
	var child = '';
	var hasPermissions = false;
	if(obj.type){
		// Build the attributes
		var attrLength = (obj.attributes)? objKeys(obj.attributes).length: 0;
		for(var i=0; i<attrLength; i++){
			var k = objKeys(obj.attributes)[i];
			hasPermissions = (k=="data-permissions")? obj.attributes[k]: false;
			attr += ' '+k+'="'+obj.attributes[k]+'"';
		}
		
		// Build any child elements
		var childLength = (obj.content)? obj.content.length: 0;
		for(var i=0; i<childLength; i++){
			if(obj.content[i]!=' '){
				child += jsonToHTML(obj.content[i],roles);
			}
		}
		
		// Build the element and nest the children
		if(hasPermissions===false || roles.indexOf(hasPermissions)!==-1 || roles.indexOf("Admin")!==-1){
			itm += '<'+obj.type+attr+'>';
			itm += child
			itm += '</'+obj.type+'>';
		}
		
		return itm;
	}else{
		// Return the item caption
		return (obj)? obj: '';
	}
}

function objKeys(obj){
	var keys = [];
	for(var k in obj) keys.push(k);
	return keys;
}