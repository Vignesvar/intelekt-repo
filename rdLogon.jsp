<!DOCTYPE html>
<html lang="en">
<head>
  
  <link rel="icon" type="image/png" href="http://108.166.44.29/opentaps_images/fio.gif">
  <title>Group Fio</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <META http-equiv="X-UA-Compatible" content="IE=edge" />
    <LINK rel="stylesheet" type="text/css" href="css/rdQuicktip.css" />
	<LINK rel="stylesheet" type="text/css" href="Framework.bootstrap.bootstrap.css" />
    <LINK rel="stylesheet" type="text/css" href="css/rdGridSystem.css" />
    <LINK rel="stylesheet" type="text/css" href="css/rdPopupPanel.css" />
    <LINK rel="stylesheet" type="text/css" href="css/rdInputCheckboxList.css" />
    <LINK rel="stylesheet" type="text/css" href="css/Signal/Theme.css" />
    <LINK rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />
    <LINK rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
    <LINK rel="stylesheet" type="text/css" href="css/themify-icons.css" />
    <LINK rel="stylesheet" type="text/css" href="css/Theme.css" />
    <LINK rel="stylesheet" type="text/css" href="css/smartmenus.css" />
    <LINK rel="stylesheet" type="text/css" href="css/daterangepicker.css" />
    <LINK rel="stylesheet" type="text/css" href="css/bootstrap-datepicker.css" />
    <LINK rel="stylesheet" type="text/css" href="css/colpick.css" />
    <LINK rel="stylesheet" type="text/css" href="css/iconpicker.min.css" />
    <LINK rel="stylesheet" type="text/css" href="_Themes/Accelerate/css/application.css?v=1" />
    <LINK rel="stylesheet" type="text/css" href="css/_theme_files.Custom.css" />



<style>


.img {
    position: relative !important;
    padding-top: 6px !important;
    padding-bottom: 6px !important;
	padding-left: 9%;
}
.header {
    border-bottom: 0px solid #e5e5e5 !important;
}
.header-logo {
    padding: 0px 0 !important;
}
.first{
background:	#EFF3F5 !important;
/*height:175px;*/
width: 360px;
    margin: 0px auto;
    border-radius: 5px;
    padding: 2em 1em !important;


}

.first1{

height:15px;
}
h3{

    text-align: center;
    padding-top: 20px;
}

}

@media(max-width:768px)
{
.first
{
width:100px !important;
}
}

</style>

</head>


<% 
		String returnUrl = request.getQueryString();
		if ((returnUrl == null) || (returnUrl.trim().length() == 0)) {
		session.setAttribute("rdLogonReturnUrl",(Object)"rdPage.aspx");
		}
		else {
		session.setAttribute("rdLogonReturnUrl",(Object)returnUrl);
	}
	%>




<body onkeypress="if (event.keyCode==13){frmLogon.submit()}" onload="document.getElementById('rdUsername').focus()" >	
  <div class="header header-logo">
    <DIV id="Header">
                    
                        <DIV id="Header-inner">
                            
                                <DIV  CLASS="navbar navbar-default  navbar-inverse" style="background:#2B5382;     border-color: #2B5382;">
                                    <DIV CLASS="container-fluid" id="container">
                                        <DIV id="navbar-header" CLASS="navbar-header">
                                           
                                                <DIV id="navbar-brand" CLASS="navbar-brand">
                                                    <IMG src="https://www.groupfio.com/wp-content/themes/agencies/images/logo.png" alt="" id="logo" CLASS="main-logo"></IMG>
                                                </DIV>
                                           
                                            
                                                <DIV id="navbar-brand" CLASS="navbar-brand-txt">
                                                    
                                                        <SPAN id="title"><SPAN class = "h5" style="font-size: 18px;">Intele</SPAN><SPAN class = "h5" style="color:#B51010;font-size:18px;">K</SPAN><SPAN class = "h5" style="font-size:18px;">t</SPAN></SPAN>
                                                    
                                                </DIV>
                                            
                                            <Button CLASS="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" onclick="return false;"><i class="fa fa-caret-down"></i>
                                            </Button>
                                        </DIV>
                                        <DIV id="navbar-collapse" CLASS="navbar-collapse collapse">
                                            <ul id="BuildActiveJMenuHeader" CLASS="nav navbar-nav navbar-right"></ul>
                                            <ul id="logout-menu" CLASS="nav navbar-nav navbar-right">
                                              
                                            </ul>
                                           
                                                <SPAN id="HeaderAdminMenu"><SPAN  id="user-menu">
</SPAN></SPAN>
                                            
                                        </DIV>
                                    </DIV>
                                </DIV>
                            
                        </DIV>
                    
                </DIV>

       <div class="first2"></div>
	   <div class="header1">
	     <h3>Login</h3></div>
	  
	   
  
  
    <div class="first1"></div>
	 <div class="first">
	 
	   <div class="form-body" style="padding:10px;padding-top:20px";>
	   <form id="frmLogon" action='<%= (String)session.getAttribute("rdLogonReturnUrl") %>' method="post">
		
		<div class="form-group has-feedback">
		  <input class="form-control" placeholder="Username" name="rdUsername" id="rdUsername" type="text">
		  <input id="rdFormLogon" type="hidden" name="rdFormLogon" value="True"/>
		  <span class="glyphicon glyphicon-user form-control-feedback"></span>
		  </div>
		
		<div class="form-group has-feedback">
		  <input type="password" class="form-control" placeholder="Password" name="rdPassword" id="rdPassword">
		  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
		  </div>
		
		<input id="Submit1" type="submit" value="Login" class="btn btn-primary btn-block" name="Submit1" style="background: #00aaf0; border: 1px solid #00aaf0;"/>
		</form>
		</div>
	 <table>
	   <tr>
	     <td id="errorCell" colSpan="2" >
		    <% String sMessage = (String)session.getAttribute("rdLogonFailMessage"); 
														if (sMessage == null) {
															sMessage = "";
														}
														else{
													   out.print(sMessage);
													   }
													%>
	 
	 
	        </td>
		</tr>
		</table>
	 
	  </div>
  
            <div style="clear: both; padding-top: 100px;">
   <div style="text-align:center;font-size: 10px;text-decoration: none;">
   <img src="fio_logo.gif" height="40px">
   			Group FiO #2020<span id="quarterYear" style="display:none"></span>
    </div>
    <div style="text-align:center;font-size: 10px;text-decoration: none;">
    	     Powered by <a href="http://www.groupfio.com/"><strong>Group FiO.</strong></a>
    </div>
</div>








</body>
</html>