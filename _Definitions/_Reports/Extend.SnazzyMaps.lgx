﻿<?xml version="1.0" encoding="utf-8"?>
<Report
	Caption="Snazzy Maps | @Constant.TitleCaption~"
	ID="Extend.SnazzyMaps"
	>
	<MasterReport
		Report="MasterReport"
	/>
	<IncludeScriptFile
		IncludedScriptFile="js.MapThemes.js"
	/>
	<IncludeScriptFile
		IncludedScriptFile="js.highlight.min.js"
	/>
	<Body>
		<Division
			Class="container-fluid"
			HtmlDiv="True"
			>
			<Division
				Class="display-1 border-bottom pb-3"
				HtmlDiv="True"
				>
				<Label
					Caption="Snazzy Maps"
					Format="HTML"
					HtmlTag="H1"
					>
					<Note
						Note="http://snazzymaps.com/"
					/>
				</Label>
				<Label
					Caption="Repository of different styles for Google Maps aimed towards web designers and developers."
					Class="lead mt-2 mb-0"
					Format="HTML"
					HtmlTag="P"
				/>
			</Division>
			<IncludeSharedElement
				DefinitionFile="Shared.Elements"
				SharedElementID="_shrdRequirements"
				>
				<PassedSharedElement
					ID="pseRequirements"
					>
					<DataLayer
						ID="dlRequirement"
						Type="Static"
						>
						<StaticDataRow
							Caption="Set [MapElementId] in JavaScript"
							FileName=""
						/>
						<StaticDataRow
							Caption="Go To SnazzyMaps.com"
							FileName="SnazzyMaps.com"
						/>
						<StaticDataRow
							Caption="Generate json for map styles using &lt;a href=&quot;void(0)&quot; onclick=&quot;javascript:window.open(&apos;http://snazzymaps.com/&apos;,&apos;_blank&apos;); return false;&quot; target=&quot;_blank&quot;&gt;SnazzyMaps&lt;/a&gt;"
							FileName=""
						/>
						<StaticDataRow
							Caption="Paste json into [SnazzyMapsJsonHere]"
							FileName=""
							showInfo=""
						/>
						<StaticDataRow
							Caption="Click on Sample to copy to clipboard "
							FileName=""
							showInfo="True"
						/>
						<SequenceColumn
							ID="sqCol"
						/>
					</DataLayer>
				</PassedSharedElement>
			</IncludeSharedElement>
			<Division
				Class="callout callout-primary pb-2"
				HtmlDiv="True"
				ID="dvInfo"
				>
				<Label
					Caption="Simple Example"
					HtmlTag="H4"
				/>
				<Remark>
					<Division
						ID="bootstrapDocumentation"
						Tooltip="Click here for Bootstrap panel documentation"
						>
						<Label
							Class="fa fa-book pull-right fa-2x"
							Format="HTML"
						/>
						<Action
							Javascript="window.open(&apos;http://getbootstrap.com/components/#panels&apos;,&apos;_blank&apos;); return false;"
							Type="Javascript"
						/>
					</Division>
				</Remark>
				<Division
					HtmlDiv="True"
					>
					<Label
						Caption="The &lt;strong&gt;rdCreated&lt;/strong&gt; event fires after the Google Map object is created. It has four properties:"
						Class="text-info"
						Format="HTML"
						HtmlTag="P"
					/>
					<DataList
						ID="listOptions"
						Ordered="False"
						>
						<DataLayer
							ID="sdlOptions"
							Type="Static"
							>
							<StaticDataRow
								caption="&lt;code&gt;map&lt;/code&gt; - Gets a reference to the newly-created Google Map object."
							/>
							<StaticDataRow
								caption="&lt;code&gt;id:&lt;/code&gt; - Gets the ID of the newly-created Google Map object."
							/>
							<StaticDataRow
								caption="&lt;code&gt;mapOptions:&lt;/code&gt; - Gets the Google Map&apos;s MapOptions object properties, ranging from background color to zoom control options. A complete list of these is available from Google on their specifications page."
							/>
							<StaticDataRow
								caption="&lt;code&gt;container:&lt;/code&gt; - Gets the ID of the internal division created as a container for the Google Map object."
							/>
						</DataLayer>
						<ListItem>
							<Label
								Caption="@Data.caption~"
								Class="code-inline"
								Format="HTML"
							/>
						</ListItem>
					</DataList>
					<Label
						Caption="This event and its properties allow you to customize the map after it&apos;s been created:"
						Class="text-info"
						Format="HTML"
						HtmlTag="P"
					/>
					<Division
						Class="copy-wrapper"
						HtmlDiv="True"
						>
						<Label
							Class="btnCopy ti ti-files"
							>
							<Action
								Javascript="void(0)"
								Type="Javascript"
							/>
						</Label>
						<Label
							Caption="var mapDiv = Y.one(&apos;#MapElementIdHere&apos;);
mapDiv.on(&apos;rdCreated&apos;, function(e) {
    e.map.setOptions({
        styles: {[SnazzyMapsJsonHere]}
    });
});"
							Class="JavaScript px-2 rounded"
							Format="HTML"
							HtmlTag="pre"
						/>
						<Label
							Caption="&lt;IncludeScript IncludedScript=&quot;mapDiv.on(&apos;rdCreated&apos;, function(e) {&amp;#xD;&amp;#xA;    e.map.setOptions({&amp;#xD;&amp;#xA;        styles: {[SnazzyMapsJsonHere]}&amp;#xD;&amp;#xA;    });&amp;#xD;&amp;#xA;});&quot; /&gt;"
							Class="ThemeHidden"
							ID="copyXMLElement"
						/>
					</Division>
				</Division>
				<Remark>
					<Label
						Caption="Visit &lt;a href=&quot;void(0)&quot; onclick=&quot;javascript:window.open(&apos;http://snazzymaps.com/&apos;,&apos;_blank&apos;); return false;&quot; target=&quot;_blank&quot;&gt;Snazzymaps.com&lt;/a&gt; for more styling examples"
						Class="form-text text-muted"
						Format="HTML"
						HtmlTag="P"
					/>
				</Remark>
			</Division>
			<Division
				Class="card card-default mb-3"
				HtmlDiv="True"
				>
				<Division
					Class="card-header"
					HtmlDiv="True"
					>
					<Label
						Caption="Example"
						Class="h5"
					/>
				</Division>
				<Division
					Class="card-body"
					HtmlDiv="True"
					>
					<DataList
						Class="nav nav-tabs"
						ID="btnStyles"
						>
						<DataLayer
							ID="sdlOrdered"
							Type="Static"
							>
							<StaticDataRow
								caption="Initial Theme"
								class="InitMapStyle"
								liClass="active"
							/>
							<StaticDataRow
								caption="Assassin&apos;s Creed IV"
								class="AssassinsCreed"
							/>
							<StaticDataRow
								caption="Monopoly"
								class="Monopoly"
							/>
							<StaticDataRow
								caption="Tactical"
								class="Tactical"
							/>
							<StaticDataRow
								caption="Pirate Map"
								class="PirateMap"
							/>
							<StaticDataRow
								caption="Lost in the Desert"
								class="LostintheDesert"
							/>
							<StaticDataRow
								caption="Zombie Survival Map"
								class="ZombieSurvivalMap"
							/>
							<StaticDataRow
								caption="Red Alert"
								class="RedAlert"
							/>
							<StaticDataRow
								caption="The Propia Effect"
								class="ThePropiaEffect"
							/>
						</DataLayer>
						<ListItem
							Class="nav-item @Data.class~"
							>
							<Label
								Caption="@Data.caption~"
								Class="nav-link @Data.liClass~"
								HtmlTag="a"
								>
								<EventHandler
									DhtmlEvent="href"
									>
									<Action
										Javascript="void(0)"
										Type="Javascript"
									/>
								</EventHandler>
							</Label>
						</ListItem>
					</DataList>
					<Division
						Class="tab-content pt-3"
						HtmlDiv="True"
						>
						<ResponsiveRow
							CollisionBehavior="Overlap"
							>
							<ResponsiveColumn
								Class="col-md-offset-1"
								ColspanLargeScreen="10"
								ColspanMediumScreen="10"
								>
								<Division
									Class="tab-pane active"
									HtmlDiv="True"
									>
									<Division
										Class="copy-wrapper"
										HtmlDiv="True"
										ID="divMap"
										>
										<Remark>
											<Label
												Caption="&lt;i class=&quot;ti ti-files&quot;&gt;&lt;/i&gt; Copy current map theme"
												Class="btnCopy"
												Format="HTML"
												>
												<Action
													Javascript="void(0)"
													Type="Javascript"
												/>
											</Label>
										</Remark>
										<GoogleMap
											ConnectionID="connGmaps"
											Height="450"
											ID="gMapStying"
											Width="100%"
											>
											<GoogleMapInitialView
												GoogleMapZoomLevel="4"
												Latitude="37.2756583"
												Longitude="-99"
											/>
										</GoogleMap>
										<IncludeScript
											ID="initStyledMap"
											IncludedScript="var mapDiv = Y.one(&apos;#gMapStying&apos;);
mapDiv.on(&apos;rdCreated&apos;, function(e) {
    e.map.setOptions({
		styles: InitMapStyle
	});
});"
										/>
										<InputHidden
											DefaultValue="InitMapStyle"
											ID="hiddenCurrentMapTheme"
										/>
									</Division>
								</Division>
							</ResponsiveColumn>
						</ResponsiveRow>
					</Division>
				</Division>
				<Division
					Class="card-footer p-0"
					HtmlDiv="True"
					>
					<Division
						HtmlDiv="True"
						ID="dvCurrentTheme"
						>
						<Remark>
							<Label
								Caption="JSON"
								Class="ThemeBold"
							/>
						</Remark>
						<Division
							Class="copy-wrapper"
							HtmlDiv="True"
							>
							<Label
								Class="btnCopy ti ti-files"
								>
								<Action
									Javascript="void(0)"
									Type="Javascript"
								/>
							</Label>
							<Label
								Caption="[{&quot;featureType&quot;:&quot;all&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;weight&quot;:&quot;2.00&quot;}]},{&quot;featureType&quot;:&quot;all&quot;,&quot;elementType&quot;:&quot;geometry.stroke&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#9c9c9c&quot;}]},{&quot;featureType&quot;:&quot;all&quot;,&quot;elementType&quot;:&quot;labels.text&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;on&quot;}]},{&quot;featureType&quot;:&quot;landscape&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#f2f2f2&quot;}]},{&quot;featureType&quot;:&quot;landscape&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#ffffff&quot;}]},{&quot;featureType&quot;:&quot;landscape.man_made&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#ffffff&quot;}]},{&quot;featureType&quot;:&quot;poi&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;road&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;lightness&quot;:45},{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;transit&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#46bcec&quot;},{&quot;visibility&quot;:&quot;on&quot;}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#c8d7d4&quot;}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;elementType&quot;:&quot;labels.text.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#070707&quot;}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;elementType&quot;:&quot;labels.text.stroke&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#ffffff&quot;}]}];"
								Class="JSON"
								HtmlTag="pre"
							/>
							<Label
								Caption="&lt;IncludeScript IncludedScript=&quot;mapDiv.on(&apos;rdCreated&apos;, function(e) {&amp;#xD;&amp;#xA;    e.map.setOptions({&amp;#xD;&amp;#xA;        styles: {[{&quot;featureType&quot;:&quot;all&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;weight&quot;:&quot;2.00&quot;}]},{&quot;featureType&quot;:&quot;all&quot;,&quot;elementType&quot;:&quot;geometry.stroke&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#9c9c9c&quot;}]},{&quot;featureType&quot;:&quot;all&quot;,&quot;elementType&quot;:&quot;labels.text&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;on&quot;}]},{&quot;featureType&quot;:&quot;landscape&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#f2f2f2&quot;}]},{&quot;featureType&quot;:&quot;landscape&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#ffffff&quot;}]},{&quot;featureType&quot;:&quot;landscape.man_made&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#ffffff&quot;}]},{&quot;featureType&quot;:&quot;poi&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;road&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;saturation&quot;:-100},{&quot;lightness&quot;:45},{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;transit&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;visibility&quot;:&quot;off&quot;}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;elementType&quot;:&quot;all&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#46bcec&quot;},{&quot;visibility&quot;:&quot;on&quot;}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;elementType&quot;:&quot;geometry.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#c8d7d4&quot;}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;elementType&quot;:&quot;labels.text.fill&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#070707&quot;}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;elementType&quot;:&quot;labels.text.stroke&quot;,&quot;stylers&quot;:[{&quot;color&quot;:&quot;#ffffff&quot;}]}]}&amp;#xD;&amp;#xA;    });&amp;#xD;&amp;#xA;});&quot; /&gt;"
								Class="ThemeHidden"
								ID="copyXMLElement"
							/>
						</Division>
					</Division>
				</Division>
			</Division>
		</Division>
		<IncludeScript
			IncludedScript="$(document).ready(function() {
	$(&apos;#btnStyles a&apos;).click(function() {
		$(this).closest(&apos;ul&apos;).find(&apos;li&gt;a&apos;).removeClass(&apos;active&apos;);
		setTheme($(this).parent().attr(&apos;class&apos;).replace(&apos;nav-item &apos;, &apos;&apos;).trim());
		$(this).closest(&apos;li&gt;a&apos;).addClass(&apos;active&apos;);
	});
});
function setTheme(theme) {
	rdGetGMapObject(&apos;gMapStying&apos;).setOptions({
		styles: window[theme]
	});
	$(&apos;#hiddenCurrentMapTheme&apos;).val(theme);
	var strJson = JSON.stringify(window[theme]);
	$(&apos;#dvCurrentTheme #copyXMLElement&apos;).text(&apos;&lt;IncludeScript IncludedScript=&quot;var mapDiv = Y.one(\&apos;#MapElementIdHere\&apos;); mapDiv.on(\&apos;rdCreated\&apos;, function(e) {e.map.setOptions({&amp;#xD;&amp;#xA;        styles: {&apos;+strJson+&apos;}&amp;#xD;&amp;#xA;    });&amp;#xD;&amp;#xA;});&quot; /&gt;&apos;);
	$(&apos;.JSON&gt;.hljs&apos;).text(strJson);
	
	$(&apos;.JSON&gt;.hljs&apos;).each(function(i, block) {
		hljs.highlightBlock(block);
	});
}"
		/>
	</Body>
	<ideTestParams/>
</Report>
