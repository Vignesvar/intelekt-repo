﻿<?xml version="1.0" encoding="utf-8"?>
<Report
	Caption="Shadows | @Constant.TitleCaption~"
	ID="Extend.Shadow"
	>
	<MasterReport
		Report="MasterReport"
	/>
	<StyleSheet
		StyleSheet="css.Shadow.css"
	/>
	<Body>
		<Division
			Class="container-fluid"
			HtmlDiv="True"
			>
			<Division
				Class="display-1 border-bottom pb-3"
				HtmlDiv="True"
				>
				<Label
					Caption="Shadow"
					HtmlTag="H1"
				/>
			</Division>
			<IncludeSharedElement
				DefinitionFile="Shared.Elements"
				SharedElementID="_shrdRequirements"
				>
				<PassedSharedElement
					ID="pseRequirements"
					>
					<DataLayer
						ID="dlRequirement"
						Type="Static"
						>
						<StaticDataRow
							Caption="Click here to download Shadow.css"
							FileName="css.Shadow"
							popupContent="&lt;StyleSheet ShowModes=&quot;rdBrowser&quot; StyleSheet=&quot;css.Shadow.css&quot; /&gt;"
						/>
						<SequenceColumn
							ID="sqCol"
						/>
					</DataLayer>
				</PassedSharedElement>
			</IncludeSharedElement>
			<Division
				Class="callout callout-info pb-2"
				HtmlDiv="True"
				>
				<DataList
					Class="list-unstyled pl-3 mb-0"
					ID="listFeatures"
					Ordered="False"
					>
					<DataLayer
						ID="sdlFeatures"
						Type="Static"
						>
						<StaticDataRow
							caption="The horizontal offset"
							content="of the shadow, positive means the shadow will be on the right of the box, a negative offset will put the shadow on the left of the box."
							pre="box-shadow: &lt;strong&gt;0&lt;/strong&gt; 5px 15px 0 rgba(0,0,0,.4) inset;"
						/>
						<StaticDataRow
							caption="The vertical offset"
							content="of the shadow, a negative one means the box-shadow will be above the box, a positive one means the shadow will be below the box."
							pre="box-shadow: 0 &lt;strong&gt;5px&lt;/strong&gt; 15px 0 rgba(0,0,0,.4) inset;"
						/>
						<StaticDataRow
							caption="The blur radius"
							content="(optional), if set to 0 the shadow will be sharp, the higher the number, the more blurred it will be."
							pre="box-shadow: 0 5px &lt;strong&gt;15px&lt;/strong&gt; 0 rgba(0,0,0,.4) inset;"
						/>
						<StaticDataRow
							caption="The spread radius"
							content="(optional), positive values increase the size of the shadow, negative values decrease the size. Default is 0 (the shadow is same size as blur)."
							pre="box-shadow: 0 5px 15px &lt;strong&gt;0&lt;/strong&gt; rgba(0,0,0,.4) inset;"
						/>
						<StaticDataRow
							caption="Color"
							content="(Optional), The color of the shadow. The default value is black. Look at CSS Color Values for a complete list of possible color values."
							pre="box-shadow: 0 5px 15px 0 &lt;strong&gt;rgba(0,0,0,.4)&lt;/strong&gt; inset;"
						/>
						<StaticDataRow
							caption="Inner Shadow"
							content="(Optional)"
							pre="box-shadow: 0 5px 15px 0 rgba(0,0,0,.4) &lt;strong&gt;inset&lt;/strong&gt;;"
						/>
					</DataLayer>
					<ListItem
						Class="pb-1"
						>
						<Division
							HtmlDiv="True"
							>
							<Label
								Caption="@Data.caption~"
								Class="ThemeBold text-dark"
								Format="HTML"
							/>
							<Label
								Caption="@Data.content~"
								Class="code-inline ml-1 text-secondary"
								Format="HTML"
							/>
						</Division>
						<Label
							Caption="@Data.pre~"
							Class="css mb-1"
							Format="HTML"
							HtmlTag="Pre"
						/>
					</ListItem>
				</DataList>
			</Division>
			<Division
				Class="callout callout-primary pt-3 pb-2"
				HtmlDiv="True"
				>
				<Label
					Caption="Example"
					Class="text-left"
					HtmlTag="H4"
				/>
				<Division
					Class="mx-3"
					HtmlDiv="True"
					>
					<Label
						Caption="Logi XML"
						Class="ThemeBold"
					/>
					<Division
						Class="copy-wrapper"
						HtmlDiv="True"
						ID="copy"
						>
						<Label
							Class="btnCopy ti-files"
							ID="btnCopy"
							>
							<Action
								Javascript="void(0)"
								Type="Javascript"
							/>
						</Label>
						<Label
							Caption="&lt;Division Class=&quot;box {shadow class}&quot;&gt;
  &lt;Label Caption=&quot;shadow-xs&quot; /&gt;
&lt;/Division&gt;"
							Class="html"
							HtmlTag="pre"
						/>
						<Label
							Caption="&lt;Division Class=&quot;box shadow-xs&quot;&gt;
  &lt;Label Caption=&quot;shadow-xs&quot; /&gt;
&lt;/Division&gt;"
							Class="ThemeHidden"
							ID="copyXMLElement"
						/>
					</Division>
				</Division>
			</Division>
			<FieldsetBox
				Caption="Basic"
				CaptionClass="h3 border-bottom pb-1"
				>
				<ResponsiveRow
					Class="col-5ths"
					CollisionBehavior="Overlap"
					>
					<ResponsiveColumn
						ColspanLargeScreen="2"
						ColspanMediumScreen="2"
						ColspanSmallScreen="2"
						>
						<Division
							Class="box shadow-xs"
							>
							<Label
								Caption="shadow-xs"
							/>
						</Division>
					</ResponsiveColumn>
					<ResponsiveColumn
						ColspanLargeScreen="2"
						ColspanMediumScreen="2"
						ColspanSmallScreen="2"
						>
						<Division
							Class="box shadow-sm"
							>
							<Label
								Caption="shadow-sm"
							/>
						</Division>
					</ResponsiveColumn>
					<ResponsiveColumn
						ColspanLargeScreen="2"
						ColspanMediumScreen="2"
						ColspanSmallScreen="2"
						>
						<Division
							Class="box shadow"
							>
							<Label
								Caption="shadow"
							/>
						</Division>
					</ResponsiveColumn>
					<ResponsiveColumn
						ColspanLargeScreen="2"
						ColspanMediumScreen="2"
						ColspanSmallScreen="2"
						>
						<Division
							Class="box shadow-lg"
							>
							<Label
								Caption="shadow-xs"
							/>
						</Division>
					</ResponsiveColumn>
					<ResponsiveColumn
						ColspanLargeScreen="2"
						ColspanMediumScreen="2"
						ColspanSmallScreen="2"
						>
						<Division
							Class="box shadow-xl"
							>
							<Label
								Caption="shadow-xl"
							/>
						</Division>
					</ResponsiveColumn>
				</ResponsiveRow>
			</FieldsetBox>
			<FieldsetBox
				Caption="Hover"
				CaptionClass="h3 border-bottom pb-1"
				>
				<ResponsiveRow
					Class="col-5ths"
					CollisionBehavior="Overlap"
					>
					<ResponsiveColumn
						ColspanLargeScreen="2"
						ColspanMediumScreen="2"
						ColspanSmallScreen="2"
						>
						<Division
							Class="box shadow-xs-hover"
							>
							<Label
								Caption="shadow-xs-hover"
							/>
						</Division>
					</ResponsiveColumn>
					<ResponsiveColumn
						ColspanLargeScreen="2"
						ColspanMediumScreen="2"
						ColspanSmallScreen="2"
						>
						<Division
							Class="box shadow-xs shadow-sm-hover"
							>
							<Label
								Caption="shadow-sm-hover"
							/>
						</Division>
					</ResponsiveColumn>
					<ResponsiveColumn
						ColspanLargeScreen="2"
						ColspanMediumScreen="2"
						ColspanSmallScreen="2"
						>
						<Division
							Class="box shadow-sm shadow-hover"
							>
							<Label
								Caption="shadow-hover"
							/>
						</Division>
					</ResponsiveColumn>
					<ResponsiveColumn
						ColspanLargeScreen="2"
						ColspanMediumScreen="2"
						ColspanSmallScreen="2"
						>
						<Division
							Class="box shadow-sm shadow-lg-hover"
							>
							<Label
								Caption="shadow-lg-hover"
							/>
						</Division>
					</ResponsiveColumn>
					<ResponsiveColumn
						ColspanLargeScreen="2"
						ColspanMediumScreen="2"
						ColspanSmallScreen="2"
						>
						<Division
							Class="box shadow-sm shadow-xl-hover"
							>
							<Label
								Caption="shadow-xl-hover"
							/>
						</Division>
					</ResponsiveColumn>
				</ResponsiveRow>
			</FieldsetBox>
			<FieldsetBox
				Caption="Alignment"
				CaptionClass="h3 border-bottom pb-1"
				>
				<ResponsiveRow
					Class="col-5ths"
					CollisionBehavior="Overlap"
					>
					<ResponsiveColumn
						ColspanLargeScreen="2"
						ColspanMediumScreen="2"
						ColspanSmallScreen="2"
						>
						<Division
							Class="box shadow-top"
							>
							<Label
								Caption="shadow-top"
							/>
						</Division>
					</ResponsiveColumn>
					<ResponsiveColumn
						ColspanLargeScreen="2"
						ColspanMediumScreen="2"
						ColspanSmallScreen="2"
						>
						<Division
							Class="box shadow-bottom"
							>
							<Label
								Caption="shadow-bottom"
							/>
						</Division>
					</ResponsiveColumn>
					<ResponsiveColumn
						ColspanLargeScreen="2"
						ColspanMediumScreen="2"
						ColspanSmallScreen="2"
						>
						<Division
							Class="box shadow-left"
							>
							<Label
								Caption="shadow-left"
							/>
						</Division>
					</ResponsiveColumn>
					<ResponsiveColumn
						ColspanLargeScreen="2"
						ColspanMediumScreen="2"
						ColspanSmallScreen="2"
						>
						<Division
							Class="box shadow-right"
							>
							<Label
								Caption="shadow-right"
							/>
						</Division>
					</ResponsiveColumn>
					<ResponsiveColumn
						ColspanLargeScreen="2"
						ColspanMediumScreen="2"
						ColspanSmallScreen="2"
						>
						<Division
							Class="box shadow-spread"
							>
							<Label
								Caption="shadow-spread"
							/>
						</Division>
					</ResponsiveColumn>
				</ResponsiveRow>
			</FieldsetBox>
			<FieldsetBox
				Caption="Inner Shadow"
				CaptionClass="h3 border-bottom pb-1"
				>
				<ResponsiveRow
					Class="col-5ths"
					CollisionBehavior="Overlap"
					>
					<ResponsiveColumn
						ColspanLargeScreen="2"
						ColspanMediumScreen="2"
						ColspanSmallScreen="2"
						>
						<Division
							Class="box shadow-inset"
							>
							<Label
								Caption="shadow-inset"
							/>
						</Division>
					</ResponsiveColumn>
					<ResponsiveColumn
						ColspanLargeScreen="2"
						ColspanMediumScreen="2"
						ColspanSmallScreen="2"
						>
						<Division
							Class="box shadow-inset-top"
							>
							<Label
								Caption="shadow-inset-top"
							/>
						</Division>
					</ResponsiveColumn>
					<ResponsiveColumn
						ColspanLargeScreen="2"
						ColspanMediumScreen="2"
						ColspanSmallScreen="2"
						>
						<Division
							Class="box shadow-inset-bottom"
							>
							<Label
								Caption="shadow-inset-bottom"
							/>
						</Division>
					</ResponsiveColumn>
					<ResponsiveColumn
						ColspanLargeScreen="2"
						ColspanMediumScreen="2"
						ColspanSmallScreen="2"
						>
						<Division
							Class="box shadow-inset-left"
							>
							<Label
								Caption="shadow-inset-left"
							/>
						</Division>
					</ResponsiveColumn>
					<ResponsiveColumn
						ColspanLargeScreen="2"
						ColspanMediumScreen="2"
						ColspanSmallScreen="2"
						>
						<Division
							Class="box shadow-inset-right"
							>
							<Label
								Caption="shadow-inset-right"
							/>
						</Division>
					</ResponsiveColumn>
				</ResponsiveRow>
			</FieldsetBox>
			<IncludeScript
				IncludedScript="$(document).ready(function() {
	var copyShadows = document.querySelectorAll(&apos;.box&apos;);
	$.getScript(&quot;_SupportFiles/clipboard.clipboard.min.js&quot;, function() {
		for (var i = 0; i &lt; copyShadows.length; i++) {
			clipboard = new Clipboard(copyShadows[i], {
				text: function(trigger) {
					return &apos;&lt;Division Class=&quot;&apos; + $(trigger).attr(&apos;class&apos;) + &apos;&quot;&gt;&lt;Label Caption=&quot;&apos; + $(trigger).find(&apos;span&apos;).text() + &apos;&quot; /&gt;&lt;/Division&gt;&apos;;
				}});
			clipboard.on(&apos;success&apos;, function(e) {
				showTooltip(e.trigger, &apos;Css has been copied&apos;);
			});
			copyShadows[i].addEventListener(&apos;mouseleave&apos;, function(e) {
				e.currentTarget.removeAttribute(&apos;aria-label&apos;);
			});
			setupTooltipAnimation($(copyShadows[i]));
		}
	});
});"
			/>
		</Division>
	</Body>
	<ideTestParams/>
</Report>
