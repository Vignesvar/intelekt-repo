﻿<?xml version="1.0" encoding="utf-8"?>
<Report
	Caption="Containers | @Constant.TitleCaption~"
	ID="Layout.Containers"
	>
	<MasterReport
		Report="MasterReport"
	/>
	<Body>
		<Division
			Class="container-fluid"
			HtmlDiv="True"
			>
			<Division
				Class="display-1 border-bottom pb-3"
				HtmlDiv="True"
				>
				<Label
					Caption="Layout Containers and Forms"
					HtmlTag="H1"
					>
					<Note
						Note="https://getbootstrap.com/docs/4.1/layout/overview/"
					/>
				</Label>
				<Label
					Caption="Wrapping containers for responsive layouts."
					Class="lead mb-0"
					Format="HTML"
					HtmlTag="P"
				/>
			</Division>
			<IncludeSharedElement
				DefinitionFile="Shared.Elements"
				SharedElementID="_shrdRequirements"
				>
				<PassedSharedElement
					ID="pseRequirements"
					>
					<DataLayer
						ID="dlRequirement"
						Type="Static"
						>
						<IncludeSharedElement
							DefinitionFile="Shared.Elements"
							SharedElementID="_shrdFramework"
						/>
						<SequenceColumn
							ID="sqCol"
						/>
					</DataLayer>
				</PassedSharedElement>
			</IncludeSharedElement>
			<FieldsetBox
				Caption="Containers"
				CaptionClass="h2 border-bottom  pb-1"
				ID="fsbContainers"
				>
				<Division
					HtmlDiv="True"
					>
					<Label
						Caption="Requires a containing element to wrap site contents and house our grid system. You may choose one of two containers to use in your projects. Note that, due to padding and more, neither container is nestable."
						Class="form-text text-muted"
						HtmlTag="P"
					/>
				</Division>
				<Division
					Class="container-fluid"
					HtmlDiv="True"
					ID="Container-Fluid"
					>
					<Division
						Class="card mb-3"
						HtmlDiv="True"
						ID="FluidLayout"
						>
						<Division
							Class="card-header"
							HtmlDiv="True"
							>
							<Label
								Caption="Fluid Layout"
							/>
						</Division>
						<Division
							Class="card-body"
							HtmlDiv="True"
							>
							<ResponsiveRow
								CollisionBehavior="Overlap"
								>
								<ResponsiveColumn
									Class="show-grid"
									>
									<Label
										Caption="Content Area..."
									/>
								</ResponsiveColumn>
							</ResponsiveRow>
						</Division>
						<Division
							Class="card-footer p-0"
							HtmlDiv="True"
							>
							<Division
								HtmlDiv="True"
								>
								<Division
									HtmlDiv="True"
									ID="dvCopy"
									>
									<Division
										Class="copy-wrapper"
										HtmlDiv="True"
										>
										<Label
											Class="btnCopy ti-files"
											ID="btnCopy"
											>
											<Action
												Javascript="void(0)"
												Type="Javascript"
											/>
										</Label>
									</Division>
									<Label
										Caption="&lt;Division Class=&quot;container-fluid&quot; HtmlDiv=&quot;True&quot;&gt;
  &lt;Label Caption=&quot;...&quot; /&gt;
&lt;/Division&gt;"
										Class="ThemeHidden"
										ID="copyXMLElement"
									/>
									<Label
										Caption="&lt;Division Class=&quot;container-fluid&quot; HtmlDiv=&quot;True&quot;&gt;
  &lt;Label Caption=&quot;...&quot; /&gt;
&lt;/Division&gt;"
										Class="html"
										HtmlTag="pre"
									/>
								</Division>
							</Division>
						</Division>
					</Division>
				</Division>
				<Division
					Class="container"
					HtmlDiv="True"
					>
					<Division
						Class="card card-default mb-3"
						HtmlDiv="True"
						ID="boxedLayout"
						>
						<Division
							Class="card-header"
							HtmlDiv="True"
							>
							<Label
								Caption="Boxed Layout"
							/>
						</Division>
						<Division
							Class="card-body"
							HtmlDiv="True"
							>
							<ResponsiveRow
								CollisionBehavior="Overlap"
								>
								<ResponsiveColumn
									Class="show-grid"
									>
									<Label
										Caption="Content Area..."
									/>
								</ResponsiveColumn>
							</ResponsiveRow>
						</Division>
						<Division
							Class="card-footer p-0"
							HtmlDiv="True"
							>
							<Division
								HtmlDiv="True"
								>
								<Division
									HtmlDiv="True"
									ID="dvCopy"
									>
									<Division
										Class="copy-wrapper"
										HtmlDiv="True"
										>
										<Label
											Class="btnCopy ti-files"
											ID="btnCopy"
											>
											<Action
												Javascript="void(0)"
												Type="Javascript"
											/>
										</Label>
									</Division>
									<Label
										Caption="&lt;Division Class=&quot;container&quot; HtmlDiv=&quot;True&quot;&gt;
  &lt;Label Caption=&quot;...&quot; /&gt;
&lt;/Division&gt;"
										Class="ThemeHidden"
										ID="copyXMLElement"
									/>
									<Label
										Caption="&lt;Division Class=&quot;container&quot; HtmlDiv=&quot;True&quot;&gt;
  &lt;Label Caption=&quot;...&quot; /&gt;
&lt;/Division&gt;"
										Class="html"
										HtmlTag="pre"
									/>
								</Division>
							</Division>
						</Division>
					</Division>
				</Division>
			</FieldsetBox>
			<FieldsetBox
				Caption="Forms"
				CaptionClass="h2 border-bottom  pb-1"
				ID="fsbForms"
				>
				<Division
					HtmlDiv="True"
					>
					<Label
						Caption="Add &lt;code&gt;.form-inline&lt;/code&gt; to your form (which doesn&apos;t have to be a &lt;code&gt;form&lt;/code&gt;) for left-aligned and inline-block controls. This only applies to forms within viewports that are at least 768px wide."
						Class="form-text text-muted"
						Format="HTML"
						HtmlTag="P"
					/>
				</Division>
				<Division
					Class="callout callout-danger"
					HtmlDiv="True"
					>
					<Label
						Caption="May require custom widths"
						HtmlTag="H4"
					/>
					<Label
						Caption="Inputs and selects have width: 100%; applied by default in Bootstrap. Within inline forms, we reset that to width: auto; so multiple controls can reside on the same line. Depending on your layout, additional custom widths may be required."
						HtmlTag="P"
					/>
				</Division>
				<Division
					HtmlDiv="True"
					>
					<Division
						Class="card card-default mb-3"
						HtmlDiv="True"
						>
						<Division
							Class="card-header"
							HtmlDiv="True"
							>
							<Label
								Caption="Inline Example"
							/>
						</Division>
						<Division
							Class="card-body"
							HtmlDiv="True"
							>
							<Division
								Class="form-inline px-3"
								HtmlDiv="True"
								>
								<Division
									Class="form-group"
									HtmlDiv="True"
									>
									<InputText
										Caption="Username: "
										CaptionClass="form-control-label ThemeBold"
										Class="form-control"
										ID="txtUserName"
										Placeholder="Email"
									/>
								</Division>
								<Division
									Class="form-group mx-sm-3"
									HtmlDiv="True"
									>
									<InputPassword
										Caption="Password: "
										CaptionClass="form-control-label ThemeBold"
										Class="form-control"
										Placeholder="Password"
									/>
								</Division>
								<Label
									Caption="Sign in"
									Class="btn btn-primary"
									HtmlTag="a"
									>
									<EventHandler
										DhtmlEvent="href"
										>
										<Action
											Javascript="void(0)"
											Type="Javascript"
										/>
									</EventHandler>
								</Label>
							</Division>
						</Division>
						<Division
							Class="card-footer p-0"
							HtmlDiv="True"
							>
							<Division
								HtmlDiv="True"
								>
								<Division
									HtmlDiv="True"
									ID="dvCopy"
									>
									<Division
										Class="copy-wrapper"
										HtmlDiv="True"
										>
										<Label
											Class="btnCopy ti-files"
											ID="btnCopy"
											>
											<Action
												Javascript="void(0)"
												Type="Javascript"
											/>
										</Label>
									</Division>
									<Label
										Caption="&lt;Division Class=&quot;form-inline&quot; HtmlDiv=&quot;True&quot;&gt;
  &lt;Division Class=&quot;form-group&quot; HtmlDiv=&quot;True&quot;&gt;
    &lt;InputText Caption=&quot;Username: &quot; CaptionClass=&quot;form-control-label ThemeBold&quot; Class=&quot;form-control&quot; Placeholder=&quot;Email&quot; /&gt;
  &lt;/Division&gt;
  &lt;Division Class=&quot;form-group mx-sm-3&quot; HtmlDiv=&quot;True&quot;&gt;
    &lt;InputPassword Caption=&quot;Password: &quot; CaptionClass=&quot;form-control-label ThemeBold&quot; Class=&quot;form-control&quot; Placeholder=&quot;Password&quot; /&gt;
  &lt;/Division&gt;
  &lt;Label HtmlTag=&quot;a&quot; Caption=&quot;Sign in&quot; Class=&quot;btn btn-primary&quot;&gt;
    &lt;EventHandler DhtmlEvent=&quot;href&quot;&gt;
      &lt;Action Type=&quot;Javascript&quot; Javascript=&quot;void(0)&quot; /&gt;
    &lt;/EventHandler&gt;
  &lt;/Label&gt;
&lt;/Division&gt;"
										Class="ThemeHidden"
										ID="copyXMLElement"
									/>
									<Label
										Caption="&lt;Division Class=&quot;form-inline&quot; HtmlDiv=&quot;True&quot;&gt;
  &lt;Division Class=&quot;form-group&quot; HtmlDiv=&quot;True&quot;&gt;
    &lt;InputText Caption=&quot;Username: &quot; CaptionClass=&quot;form-control-label ThemeBold&quot; Class=&quot;form-control&quot; Placeholder=&quot;Email&quot; /&gt;
  &lt;/Division&gt;
  &lt;Division Class=&quot;form-group mx-sm-3&quot; HtmlDiv=&quot;True&quot;&gt;
    &lt;InputPassword Caption=&quot;Password: &quot; CaptionClass=&quot;form-control-label ThemeBold&quot; Class=&quot;form-control&quot; Placeholder=&quot;Password&quot; /&gt;
  &lt;/Division&gt;
  &lt;Label HtmlTag=&quot;a&quot; Caption=&quot;Sign in&quot; Class=&quot;btn btn-primary&quot;&gt;
    &lt;EventHandler DhtmlEvent=&quot;href&quot;&gt;
      &lt;Action Type=&quot;Javascript&quot; Javascript=&quot;void(0)&quot; /&gt;
    &lt;/EventHandler&gt;
  &lt;/Label&gt;
&lt;/Division&gt;"
										Class="html"
										HtmlTag="pre"
									/>
								</Division>
							</Division>
						</Division>
					</Division>
					<Division
						Class="card card-default mb-3"
						HtmlDiv="True"
						>
						<Division
							Class="card-header"
							HtmlDiv="True"
							>
							<Label
								Caption="Horizontal Example"
							/>
						</Division>
						<Division
							Class="card-body"
							HtmlDiv="True"
							>
							<Division
								Class="form-horizontal px-3"
								HtmlDiv="True"
								>
								<Division
									Class="form-group"
									HtmlDiv="True"
									>
									<InputText
										Caption="Username: "
										CaptionClass="form-control-label ThemeBold"
										Class="form-control"
										Placeholder="Email"
									/>
								</Division>
								<Division
									Class="form-group"
									HtmlDiv="True"
									>
									<InputPassword
										Caption="Password: "
										CaptionClass="form-control-label ThemeBold"
										Class="form-control"
										Placeholder="Password"
									/>
								</Division>
								<Label
									Caption="Sign in"
									Class="btn btn-primary"
									HtmlTag="a"
									>
									<EventHandler
										DhtmlEvent="href"
										>
										<Action
											Javascript="void(0)"
											Type="Javascript"
										/>
									</EventHandler>
								</Label>
							</Division>
						</Division>
						<Division
							Class="card-footer p-0"
							HtmlDiv="True"
							>
							<Division
								HtmlDiv="True"
								>
								<Division
									Class="copy-wrapper"
									HtmlDiv="True"
									ID="dvCopy"
									>
									<Label
										Class="btnCopy ti-files"
										ID="btnCopy"
										>
										<Action
											Javascript="void(0)"
											Type="Javascript"
										/>
									</Label>
								</Division>
								<Label
									Caption="&lt;Division Class=&quot;form-horizontal&quot; HtmlDiv=&quot;True&quot;&gt;
  &lt;Division Class=&quot;form-group&quot; HtmlDiv=&quot;True&quot;&gt;
    &lt;InputText Caption=&quot;Username: &quot; CaptionClass=&quot;form-control-label ThemeBold&quot; Class=&quot;form-control&quot; Placeholder=&quot;Email&quot; /&gt;
  &lt;/Division&gt;
  &lt;Division Class=&quot;form-group&quot; HtmlDiv=&quot;True&quot;&gt;
    &lt;InputPassword Caption=&quot;Password: &quot; CaptionClass=&quot;form-control-label ThemeBold&quot; Class=&quot;form-control&quot; Placeholder=&quot;Password&quot; /&gt;
  &lt;/Division&gt;
  &lt;Label HtmlTag=&quot;a&quot; Caption=&quot;Sign in&quot; Class=&quot;btn btn-primary&quot;&gt;
    &lt;EventHandler DhtmlEvent=&quot;href&quot;&gt;
      &lt;Action Type=&quot;Javascript&quot; Javascript=&quot;void(0)&quot; /&gt;
    &lt;/EventHandler&gt;
  &lt;/Label&gt;
&lt;/Division&gt;"
									Class="ThemeHidden"
									ID="copyXMLElement"
								/>
								<Label
									Caption="&lt;Division Class=&quot;form-horizontal&quot; HtmlDiv=&quot;True&quot;&gt;
  &lt;Division Class=&quot;form-group&quot; HtmlDiv=&quot;True&quot;&gt;
    &lt;InputText Caption=&quot;Username: &quot; CaptionClass=&quot;form-control-label ThemeBold&quot; Class=&quot;form-control&quot; Placeholder=&quot;Email&quot; /&gt;
  &lt;/Division&gt;
  &lt;Division Class=&quot;form-group&quot; HtmlDiv=&quot;True&quot;&gt;
    &lt;InputPassword Caption=&quot;Password: &quot; CaptionClass=&quot;form-control-label ThemeBold&quot; Class=&quot;form-control&quot; Placeholder=&quot;Password&quot; /&gt;
  &lt;/Division&gt;
  &lt;Label HtmlTag=&quot;a&quot; Caption=&quot;Sign in&quot; Class=&quot;btn btn-primary&quot;&gt;
    &lt;EventHandler DhtmlEvent=&quot;href&quot;&gt;
      &lt;Action Type=&quot;Javascript&quot; Javascript=&quot;void(0)&quot; /&gt;
    &lt;/EventHandler&gt;
  &lt;/Label&gt;
&lt;/Division&gt;"
									Class="html"
									HtmlTag="pre"
								/>
							</Division>
						</Division>
					</Division>
				</Division>
			</FieldsetBox>
		</Division>
	</Body>
	<ideTestParams/>
</Report>
