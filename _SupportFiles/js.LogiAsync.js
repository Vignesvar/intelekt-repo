//This script triggers an asynchronous process and returns data to the app
function runProc(procURL)
{
	var userMessage = document.getElementById('linkToReport');
	userMessage.innerHTML = 'Please Wait';
	var response = httpGetAsync(procURL, function(responseData){
        var jsondata = JSON.parse(responseData);
        userMessage.innerHTML = jsondata.data[0].message;
        userMessage.href = jsondata.data[0].url;
        
    });
	
}

//This handles the actual get request to the process
function httpGetAsync(theUrl, callback)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous 
    xmlHttp.send(null);
}