//This sample displays an alert box

function showMore() {
  
  var dots = document.getElementById("dots_Row1");
  var moreText = document.querySelectorAll("#more_Row6, #more_Row7, #more_Row8, #more_Row9");
  var btnText = document.getElementById("myBtn");

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    btnText.innerHTML = "Read more";
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    btnText.innerHTML = "Read less";
    moreText.style.display = "block";
  }
}